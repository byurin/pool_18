/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval_expr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 23:36:04 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/25 23:36:09 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "eval.h"

int		ft_getnum(char **str)
{
	char	*ptr;
	int		nb;

	while (**str == ' ')
		(*str)++;
	if (**str == '(')
	{
		(*str)++;
		nb = ft_subadd(str);
		if (**str == ')')
			(*str)++;
		return (nb);
	}
	ptr = *str;
	nb = ft_atoi(*str, &ptr);
	*str = ptr;
	return (nb);
}

int		ft_modumultidiv(char **str)
{
	int		nb;
	int		nb2;
	char	op;

	nb = ft_getnum(str);
	while (1)
	{
		while (**str == ' ')
			(*str)++;
		op = **str;
		if (op != '*' && op != '/' && op != '%')
			return (nb);
		(*str)++;
		nb2 = ft_getnum(str);
		if (op == '*')
			nb *= nb2;
		else if (op == '/')
			nb /= nb2;
		else
			nb %= nb2;
	}
}

int		ft_subadd(char **str)
{
	int		nb;
	int		nb2;
	char	op;

	nb = ft_modumultidiv(str);
	while (1)
	{
		while (**str == ' ')
			(*str)++;
		op = **str;
		if (op != '-' && op != '+')
			return (nb);
		(*str)++;
		nb2 = ft_modumultidiv(str);
		nb = op == '-' ? (nb - nb2) : (nb + nb2);
	}
}

int		eval_expr(char *str)
{
	return (ft_subadd(&str));
}
