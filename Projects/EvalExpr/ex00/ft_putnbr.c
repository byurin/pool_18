/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 23:37:58 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/25 23:38:05 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "eval.h"

void	loop(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb = (nb * -1);
	}
	if (nb >= 10)
	{
		loop(nb / 10);
	}
	ft_putchar((nb % 10) + 48);
}

void	ft_putnbr(int nb)
{
	if (nb == -2147483648)
	{
		nb = -214748364;
		loop(nb);
		ft_putchar('8');
	}
	else
		loop(nb);
}
