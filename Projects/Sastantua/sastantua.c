/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/11 18:43:54 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/11 23:41:48 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		calc_base(int size)
{
	int tmp;
	int mid;

	tmp = size;
	mid = -2;
	while (--tmp >= 0)
		mid += tmp + 4 + tmp % 2 + tmp / 2;
	return (mid);
}

void	print_door(int door, int stars, int knob)
{
	int i;

	i = 0;
	while (i++ < stars)
		ft_putchar('*');
	i = 0;
	while (i++ < door)
	{
		if (door >= 5 && knob > 0)
		{
			if (i == knob - 1)
			{
				ft_putchar('$');
				i++;
			}
		}
		ft_putchar('|');
	}
	i = 0;
	while (i++ < stars)
		ft_putchar('*');
}

int		init(int spaces, int lvl_lines, int stars, int door)
{
	int i;

	i = 0;
	door = door % 2 == 0 ? door - 1 : door;
	while (lvl_lines > 0)
	{
		while (i++ < spaces)
			ft_putchar(' ');
		i = 0;
		ft_putchar('/');
		if (door < lvl_lines)
			while (i++ < stars)
				ft_putchar('*');
		else if (lvl_lines <= door && lvl_lines == (door / 2) + 1)
			print_door(door, (stars - door) / 2, door);
		else if (lvl_lines <= door)
			print_door(door, (stars - door) / 2, 0);
		i = 0;
		stars = stars + 2;
		spaces--;
		lvl_lines--;
		ft_putchar('\\');
		ft_putchar('\n');
	}
	return (stars - 2);
}

void	sastantua(int size)
{
	int spaces;
	int lvl_lines;
	int offset;
	int stars;
	int i;

	lvl_lines = 3;
	spaces = calc_base(size);
	offset = 3;
	stars = 1;
	i = 0;
	while (i < size && size > 0)
	{
		if (i == size - 1)
			stars = init(spaces, lvl_lines, stars, size);
		else
			stars = init(spaces, lvl_lines, stars, 0);
		spaces = spaces - (lvl_lines - 1);
		if (lvl_lines >= 4 && lvl_lines % 2 == 1)
			offset++;
		spaces -= offset;
		stars = stars + (offset * 2);
		lvl_lines++;
		i++;
	}
}
