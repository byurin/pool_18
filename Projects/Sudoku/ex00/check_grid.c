/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_grid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 22:22:48 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/18 22:22:51 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		check_line(char *str, char value)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] == value)
			return (1);
		i++;
	}
	return (0);
}

int		check_column(char **str, int position, char value)
{
	int i;

	i = 0;
	while (i < 9)
	{
		if (str[i][position] == value)
			return (1);
		i++;
	}
	return (0);
}

int		check_grid(char **str, int x, int y, char value)
{
	int i;
	int j;

	i = 0;
	x -= (x % 3);
	y -= (y % 3);
	while (i < 3)
	{
		j = 0;
		while (j < 3)
		{
			if (value == str[x + i][y + j])
				return (1);
			j++;
		}
		i++;
	}
	return (0);
}

int		check_whole(char **grid, int pos, char c)
{
	if (check_line(grid[X], c) == 1)
		return (1);
	if (check_column(grid, Y, c) == 1)
		return (1);
	if (check_grid(grid, X, Y, c) == 1)
		return (1);
	return (0);
}
