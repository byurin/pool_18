/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 22:24:00 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/18 23:08:41 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		main(int argc, char **argv)
{
	if (argc == 10)
	{
		argv++;
		argc--;
		if (ft_check_input(argc, argv, 0) == 1)
		{
			ft_putstr("Error\n");
			return (0);
		}
		if (get_poss(argv, 0) == 1)
			ft_putstr("Error\n");
		else
			ft_display(argv);
	}
	else if (argc != 10)
		ft_putstr("Error");
	return (0);
}
