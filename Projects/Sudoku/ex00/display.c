/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 22:23:42 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/18 22:28:59 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

void	put_line(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		if (str[i + 1] != '\0')
			ft_putchar(' ');
		i++;
	}
	ft_putchar('\n');
}

void	ft_display(char **tab)
{
	int i;

	i = 0;
	while (i < 9)
	{
		put_line(tab[i]);
		i++;
	}
}
