/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 22:24:11 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/18 22:54:46 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int		ft_check_input(int argc, char **argv, int hints_nb)
{
	int i;
	int j;

	i = 0;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j] != '\0')
		{
			if ((argv[i][j] < '1' || argv[i][j] > '9') && argv[i][j] != '.')
				return (1);
			if (argv[i][j] != '.')
			{
				hints_nb++;
				if (check_hints(argv, ((i * 10) + j)) == 1)
					return (1);
			}
			j++;
		}
		if (j != 9)
			return (1);
		i++;
	}
	return (hints_nb >= 17 ? 0 : 1);
}

int		check_hints(char **argv, int pos)
{
	char save;

	save = '.';
	save = argv[X][Y];
	argv[X][Y] = '.';
	if (check_whole(argv, pos, save) == 1)
		return (1);
	argv[X][Y] = save;
	return (0);
}
