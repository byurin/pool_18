/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudoku.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 22:24:24 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/18 23:10:12 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SUDOKU_H
# define SUDOKU_H

# include <unistd.h>

# define Y (pos % 10)
# define X (pos / 10)

int		check_line(char *str, char value);
int		check_column(char **str, int position, char value);
int		check_grid(char **str, int pos_x, int pos_y, char value);
int		check_whole(char **grid, int pos, char c);

int		get_poss(char **grid, int pos);
int		init_backtrack(char **grid, int count_poss, int pos, int save);
int		ft_rec(char **grid, int pos);

void	ft_display(char **tab);

void	ft_putchar(char c);
void	ft_putstr(char *str);
int		check_hints(char **argv, int pos);
int		ft_check_input(int argc, char **argv, int hints_nb);

#endif
