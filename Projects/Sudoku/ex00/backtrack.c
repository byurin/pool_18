/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backtrack.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 22:23:27 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/18 23:10:14 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sudoku.h"

int		forward(int pos)
{
	if (Y == 8)
	{
		pos++;
	}
	pos++;
	return (pos);
}

int		get_poss(char **grid, int pos)
{
	int save;
	int count_poss;

	save = 0;
	while (pos <= 89)
	{
		if (grid[X][Y] == '.')
		{
			count_poss = 0;
			count_poss = init_backtrack(grid, count_poss, pos, save);
			if (count_poss > 1 || count_poss == 0)
				return (1);
			else if (grid[X][Y] == '.')
				return (0);
		}
		pos = forward(pos);
	}
	return (0);
}

int		init_backtrack(char **grid, int count_poss, int pos, int save)
{
	int i;

	i = 1;
	while (i < 10)
	{
		if (check_whole(grid, pos, '0' + i) == 0)
		{
			grid[X][Y] = '0' + i;
			if (ft_rec(grid, pos) != 0)
			{
				save = i;
				count_poss++;
			}
		}
		i++;
	}
	grid[X][Y] = '0' + save;
	return (count_poss);
}

int		ft_rec(char **grid, int pos)
{
	int i;

	i = 1;
	if (pos >= 89)
		return (1);
	if (grid[X][Y] != '.')
		return (ft_rec(grid, forward(pos)));
	while (i < 10)
	{
		if (check_whole(grid, pos, '0' + i) == 0)
		{
			grid[X][Y] = '0' + i;
			if (ft_rec(grid, forward(pos)) == 1)
			{
				grid[X][Y] = '.';
				return (1);
			}
			grid[X][Y] = '.';
		}
		grid[X][Y] = '.';
		i++;
	}
	grid[X][Y] = '.';
	return (0);
}
