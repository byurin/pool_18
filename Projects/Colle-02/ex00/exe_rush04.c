/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exe_rush04.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: galric <galric@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 22:57:11 by galric            #+#    #+#             */
/*   Updated: 2018/02/25 18:50:14 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush2.h"

char	ft_crea04(int x, int y, int line, int row)
{
	if ((row == 1 && line == 1) || (row == x && line == y && x != 1 && y != 1))
		return ('A');
	if ((row == x && line == 1 && x != 1) || (row == 1 && line == y && y != 1))
		return ('C');
	if ((row > 1 && row < x) && (line == 1 || line == y))
		return ('B');
	if ((line > 1 && line < y) && (row == 1 || row == x))
		return ('B');
	if (line != 1 && row != 1 && line != y && row != x)
		return (' ');
	return (0);
}

char	*rush04(int x, int y)
{
	int		row;
	int		line;
	int		cpt;
	char	*dest;

	cpt = 0;
	row = 0;
	line = 0;
	if ((dest = malloc((sizeof(char) * (x * y) + y))) == NULL)
		return (0);
	while (++line <= y)
	{
		while (++row <= x)
		{
			dest[cpt] = ft_crea04(x, y, line, row);
			cpt++;
		}
		dest[cpt++] = '\n';
		row = 0;
	}
	return (dest);
}
