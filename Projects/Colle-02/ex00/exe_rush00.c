/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exe_rush00.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: galric <galric@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 22:57:11 by galric            #+#    #+#             */
/*   Updated: 2018/02/25 19:53:56 by galric           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush2.h"

char	ft_crea00(int x, int y, int line, int row)
{
	if ((row == 1 && line == 1 && x != 1) || (row == 1 && line == y && x != 1))
		return ('o');
	if ((row == x && line == 1) || (row == x && line == y))
		return ('o');
	if ((row > 1 && row < x) && (line == 1 || line == y))
		return ('-');
	if ((line > 1 && line < y) && (row == 1 || row == x))
		return ('|');
	if (line != 1 && row != 1 && line != y && row != x)
		return (' ');
	return (0);
}

char	*rush00(int x, int y)
{
	int		row;
	int		line;
	int		cpt;
	char	*dest;

	cpt = 0;
	row = 0;
	line = 0;
	if ((dest = malloc((sizeof(char) * (x * y) + y))) == NULL)
		return (0);
	while (++line <= y)
	{
		while (++row <= x)
		{
			dest[cpt] = ft_crea00(x, y, line, row);
			cpt++;
		}
		dest[cpt++] = '\n';
		row = 0;
	}
	return (dest);
}
