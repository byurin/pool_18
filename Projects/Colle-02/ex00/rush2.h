/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush2.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: galric <galric@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 22:13:06 by galric            #+#    #+#             */
/*   Updated: 2018/02/25 19:17:50 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RUSH2_H
# define RUSH2_H

# include <unistd.h>
# include <stdlib.h>

# define BUF_SIZE 60000

void		ft_putchar(char c);
void		ft_putstr(char *str);
void		ft_putnbr(int nb);
int			ft_strcmp(char *s1, char *s2);
char		*ft_strcpy(char *dest, char *src);
int			get_char(void);
char		*get_string(char *str, char buff[BUF_SIZE]);
char		*ft_strdup(char *dest, char *src);
char		*concat(char *tmp, char *str, int len, char buff[BUF_SIZE]);
int			ft_strlen(char *str);
char		*ft_strcat(char *dest, char *src);
char		*ft_strcpy(char *s1, char *s2);
char		*ft_realloc(char *str, int size);
char		*rush00(int x, int y);
char		*rush01(int x, int y);
char		*rush02(int x, int y);
char		*rush03(int x, int y);
char		*rush04(int x, int y);
void		ft_tabcmp_4(char *buffer, int x, int y, int cpt);
void		ft_tabcmp_2_3(char *buffer, int x, int y, int cpt);
void		ft_tabcmp_1_2(char *buffer, int x, int y);

#endif
