/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: galric <galric@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/24 22:12:08 by galric            #+#    #+#             */
/*   Updated: 2018/02/25 19:56:54 by galric           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush2.h"

int		get_char(void)
{
	char c;

	if (read(0, &c, 1) == 0)
		return (0);
	return (c);
}

char	*ft_realloc(char *str, int size)
{
	free(str);
	if ((str = malloc(sizeof(str) * size)) == NULL)
		return (0);
	if (str == NULL)
		return (NULL);
	else
		return (str);
}

char	*get_string(char *str, char buff[BUF_SIZE])
{
	int		len;
	int		c;
	char	*tmp;

	len = 0;
	c = 0;
	tmp = NULL;
	while (c >= 0)
	{
		c = get_char();
		buff[len] = (char)c;
		len++;
		if (len == BUF_SIZE - 1 || c == 0)
		{
			buff[len] = '\0';
			if (str == NULL)
				str = ft_strdup(str, buff);
			else
				str = concat(tmp, str, len, buff);
			len = 0;
		}
		c = c == 0 ? -1 : c;
	}
	return (str);
}

int		main(void)
{
	char	*str;
	char	buff[BUF_SIZE];
	int		x;
	int		y;
	int		k;

	k = 0;
	x = 0;
	y = 0;
	str = NULL;
	str = get_string(str, buff);
	while (str[k])
	{
		if (str[k] == '\n')
			y++;
		if (str[k] != '\n')
			x++;
		k++;
	}
	if (y != 0)
		x = x / y;
	ft_tabcmp_1_2(str, x, y);
	free(str);
	return (0);
}
