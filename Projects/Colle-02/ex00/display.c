/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: galric <galric@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 16:02:26 by galric            #+#    #+#             */
/*   Updated: 2018/02/25 19:12:57 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush2.h"

void	ft_disp(int x, int y)
{
	ft_putchar('[');
	ft_putnbr(x);
	ft_putchar(']');
	ft_putchar(' ');
	ft_putchar('[');
	ft_putnbr(y);
	ft_putchar(']');
}

void	ft_tabcmp_4(char *buffer, int x, int y, int cpt)
{
	if (ft_strcmp(buffer, rush04(x, y)) == 0)
	{
		if (cpt > 0)
			ft_putstr(" || ");
		ft_putstr("[colle-04] ");
		ft_disp(x, y);
		cpt++;
	}
	if (cpt == 0)
		ft_putstr("aucune");
	ft_putchar('\n');
}

void	ft_tabcmp_2_3(char *buffer, int x, int y, int cpt)
{
	if (ft_strcmp(buffer, rush02(x, y)) == 0)
	{
		if (cpt > 0)
			ft_putstr(" || ");
		ft_putstr("[colle-02] ");
		ft_disp(x, y);
		cpt++;
	}
	if (ft_strcmp(buffer, rush03(x, y)) == 0)
	{
		if (cpt > 0)
			ft_putstr(" || ");
		ft_putstr("[colle-03] ");
		ft_disp(x, y);
		cpt++;
	}
	ft_tabcmp_4(buffer, x, y, cpt);
}

void	ft_tabcmp_1_2(char *buffer, int x, int y)
{
	int	cpt;

	if (buffer[0] == '\0')
	{
		ft_putstr("aucune\n");
		return ;
	}
	cpt = 0;
	if (ft_strcmp(buffer, rush00(x, y)) == 0)
	{
		ft_putstr("[colle-00] ");
		ft_disp(x, y);
		cpt++;
	}
	if (ft_strcmp(buffer, rush01(x, y)) == 0)
	{
		if (cpt > 0)
			ft_putstr(" || ");
		ft_putstr("[colle-01] ");
		ft_disp(x, y);
		cpt++;
	}
	ft_tabcmp_2_3(buffer, x, y, cpt);
}
