/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: galric <galric@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/25 17:46:50 by galric            #+#    #+#             */
/*   Updated: 2018/02/25 19:55:17 by galric           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "rush2.h"

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

char	*ft_strcat(char *dest, char *src)
{
	int i;
	int j;

	i = ft_strlen(dest);
	j = 0;
	while (src[j] != '\0')
	{
		dest[i] = src[j];
		i++;
		j++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*ft_strdup(char *dest, char *src)
{
	int i;
	int len;

	i = 0;
	len = ft_strlen(src);
	if (!(dest = malloc(sizeof(dest) * len)))
		return (0);
	while (i < ft_strlen(src))
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char	*concat(char *tmp, char *str, int len, char buff[BUF_SIZE])
{
	tmp = ft_strdup(tmp, str);
	str = ft_realloc(str, ft_strlen(str) + len);
	str = ft_strcpy(str, tmp);
	free(tmp);
	str = ft_strcat(str, buff);
	return (str);
}
