#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

char	*ft_strupcase(char *str);

int	main(int argc, char **argv)
{
	(void)argc;

	ft_putstr("STR = ");
	ft_putstr(argv[1]);
	ft_putchar('\n');
	ft_putstr("RES = ");
	ft_putstr(ft_strupcase(argv[1]));
	ft_putchar('\n');
	ft_putchar('\n');

	return (0);
}
