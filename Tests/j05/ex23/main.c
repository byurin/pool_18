#include <unistd.h>

void	*ft_print_memory(void *addr, unsigned int size);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(void)
{
	char *str = "AGREEEEUUUUUUUUUUUUUUUHHHHHHHH";
	ft_print_memory(str, 1);
	return (0);
}
