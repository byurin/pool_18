#!/bin/bash
if [ -f test_cases.txt ]; then
	if [ -f a.out ]; then
		cat test_cases.txt | while read line; do
			./a.out "$line";
			printf '\n';
		done
	fi
fi
