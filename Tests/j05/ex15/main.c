#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb *= -1;
	}
	if (nb > 10)
		putnbr(nb / 10);
	else
		ft_putchar((nb % 10) + '0');
}

int		ft_str_is_printable(char *str);

int	main(int argc, char **argv)
{
	(void)argc;

	ft_putstr("STR = ");
	ft_putstr(argv[1]);
	ft_putchar('\n');
	ft_putstr("RES = ");
	putnbr(ft_str_is_printable(argv[1]));
	ft_putchar('\n');
	ft_putchar('\n');

	return (0);
}
