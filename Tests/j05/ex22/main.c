#include <unistd.h>

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str++)
		ft_putchar(*str);
}

void	ft_putstr_non_printable(char *str);

int		main(void)
{
	ft_putstr_non_printable("\v He \177 ll \n o \t \0");
	ft_putchar('\n');
	ft_putstr_non_printable("pcWeZLTmJJNFNaqiOkkFfjszLpVWNnfGFfzXbEVxnZhhwbbuAarycMAMidnaWJagFooMKihNFpNVIKmKFmJFXfvkcHxgxbiWVOjBwCtSzmxNaepRuOSPeDqqkPIVpwtwzpWJZuwJZZQBlupHqsOetRTHlDllWRouPiCuxWUthtsAcWmqkJbpomXgupzYFVKqGqvnOyKIsNreTDtDHizfPMUsicioojyMS\e[1;5;31mTROCK YOU42\b\42\0\0\0\0\0\0\0\0\0\0\0\0");
	ft_putchar('\n');
	ft_putstr_non_printable("\a\b\e\f\n\r\t\v");
	return (0);
}
