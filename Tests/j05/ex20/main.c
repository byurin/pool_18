#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putnbr_base(int nbr, char *base);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	ft_putnbr(long int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		n *= -1;
	}
	if (n > 9)
		ft_putnbr(n / 10);
	ft_putchar(n % 10 + '0');
}

int	main(int argc, char **argv)
{
	(void)argc;
	long int nbr = atol(argv[1]);
	char *base = argv[2];

	ft_putstr("NBR = ");
	ft_putnbr(nbr);
	ft_putstr(", BASE = ");
	ft_putstr(base);
	ft_putchar('\n');
	ft_putnbr_base(nbr, base);
	ft_putchar('\n');

	return (0);
}
