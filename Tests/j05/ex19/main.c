#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	putnbr(unsigned int nb)
{
	if (nb > 9)
		putnbr(nb / 10);
	ft_putchar(nb % 10 + '0');
}

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

int	main(int argc, char **argv)
{
	(void)argc;

	char *dest = (char*)malloc(sizeof (char) * (strlen(argv[1]) + strlen(argv[2])) + 1);
	strcpy(dest, argv[1]);
	char *src = (char*)malloc(sizeof (char) * strlen(argv[2]) + 1);
	strcpy(src, argv[2]);
	unsigned int nb = atoi(argv[3]);

	char *dest2 = (char*)malloc(sizeof (char) * (strlen(argv[1]) + strlen(argv[2])) + 1);
	strcpy(dest2, argv[1]);
	char *src2 = (char*)malloc(sizeof (char) * strlen(argv[2]) + 1);
	strcpy(src2, argv[2]);
	unsigned int nb2 = atoi(argv[3]);

	ft_putstr("DEST = ");
	ft_putstr(argv[1]);
	ft_putstr(", ");
	ft_putstr("SRC = ");
	ft_putstr(argv[2]);
	ft_putstr(", ");
	ft_putstr("SIZE = ");
	ft_putstr(argv[3]);
	ft_putstr("\n");

	ft_putstr("STRLCPY = ");
	putnbr(strlcpy(dest, src, nb));
	ft_putstr(", ");
	ft_putstr(dest);
	ft_putstr("\n");

	ft_putstr("FT_STRLCPY = ");
	putnbr(ft_strlcpy(dest2, src2, nb2));
	ft_putstr(", ");
	ft_putstr(dest2);
	ft_putstr("\n");

	free(dest);
	free(src);
	free(dest2);
	free(src2);
	return (0);
}
