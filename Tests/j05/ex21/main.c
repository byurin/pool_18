#include <string.h>
#include <stdlib.h>
#include <unistd.h>

long int	ft_atoi_base(char *str, char *base);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	ft_putnbr(int n)
{
	if (n != -2147483648)
	{
		if (n < 0)
		{
			ft_putchar('-');
			n *= -1;
		}
		if (n > 9)
			ft_putnbr(n / 10);
		ft_putchar(n % 10 + '0');
	}
	else
		ft_putstr("-2147483648");
}

int	main(int argc, char **argv)
{
	(void)argc;
	char *str = argv[1];
	char *base = argv[2];

	ft_putstr("STR = ");
	ft_putstr(str);
	ft_putstr(", BASE = ");
	ft_putstr(base);
	ft_putchar('\n');
	ft_putnbr(ft_atoi_base(str, base));
	ft_putchar('\n');

	return (0);
}
