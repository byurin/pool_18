#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb *= -1;
	}
	if (nb > 10)
		putnbr(nb / 10);
	ft_putchar(nb % 10 + '0');
}

int		ft_strncmp(char *s1, char *s2, unsigned int n);

int	main(int argc, char **argv)
{
	(void)argc;

	char *s0 = argv[1];
	char *s1 = argv[2];
	char *n1 = argv[3];

	char *s2 = (char*)malloc(sizeof (char) * strlen(argv[1]) + 1);
	strcpy(s2, argv[1]);
	char *s3 = (char*)malloc(sizeof (char) * strlen(argv[2]) + 1);
	strcpy(s3, argv[2]);
	char *n2 = (char*)malloc(sizeof (char) * strlen(argv[3]) + 1);
	strcpy(n2, argv[3]);

	ft_putstr("S1 = ");
	ft_putstr(argv[1]);
	ft_putstr(", ");
	ft_putstr("S2 = ");
	ft_putstr(argv[2]);
	ft_putstr(", ");
	ft_putstr("N = ");
	ft_putstr(argv[3]);
	ft_putstr("\n");

	ft_putstr("STRNCMP = ");
	putnbr(strncmp(s0, s1, atoi(n1)));
	ft_putstr("\n");

	ft_putstr("FT_STRNCMP = ");
	putnbr(ft_strncmp(s2, s3, atoi(n2)));
	ft_putstr("\n");

	free(s2);
	free(s3);
	free(n2);
	return (0);
}
