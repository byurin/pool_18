#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

char	*ft_strstr(char *str, char *to_find);

int	main(int argc, char **argv)
{
	(void)argc;

	char *str = argv[1];
	char *to_find = argv[2];

	char *str2 = (char*)malloc(sizeof (char) * strlen(argv[1]) + 1);
	strcpy(str2, argv[1]);
	char *to_find2 = (char*)malloc(sizeof (char) * strlen(argv[2]) + 1);
	strcpy(to_find2, argv[2]);

	ft_putstr("STR = ");
	ft_putstr(argv[1]);
	ft_putstr(", ");
	ft_putstr("FIND = ");
	ft_putstr(argv[2]);
	ft_putstr("\n");

	ft_putstr("STRSTR = ");
	if (strstr(str2, to_find2) == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(strstr(str2, to_find2));
	ft_putstr("\n");

	ft_putstr("FT_STRSTR = ");
	if (ft_strstr(str, to_find) == NULL)
		ft_putstr("NULL");
	else
		ft_putstr(ft_strstr(str, to_find));
	ft_putstr("\n");

	free(str2);
	free(to_find2);
	return (0);
}
