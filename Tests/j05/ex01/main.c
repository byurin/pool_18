#include <unistd.h>

void	ft_putnbr(int nb);

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

int		main(void)
{
	ft_putnbr(-123456789);
	ft_putchar('\n');
	ft_putnbr(-10);
	ft_putchar('\n');
	ft_putnbr(-2147483648);
	ft_putchar('\n');
	ft_putnbr(2147483647);
	ft_putchar('\n');
	return (0);
}
