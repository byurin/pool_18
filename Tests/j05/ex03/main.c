#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

char	ft_strcpy(char *dest, char *src);

int		main(int argc, char **argv)
{
	(void)argc;
	char *dest = argv[1];
	char *src = argv[2];

	char *dest2 = (char*)malloc(sizeof (char) * strlen(argv[1]));
	strcpy(dest2, argv[1]);
	char *src2 = (char*)malloc(sizeof (char) * strlen(argv[2]));
	strcpy(src2, argv[2]);

	ft_putstr("DEST = ");
	ft_putstr(argv[1]);
	ft_putstr(", ");
	ft_putstr("SRC = ");
	ft_putstr(argv[2]);
	ft_putchar('\n');

	strcpy(dest2, src2);
	ft_putstr("STRCPY == ");
	ft_putstr(dest2);
	ft_putchar('\n');

	ft_strcpy(dest, src);
	ft_putstr("FT_STRCPY == ");
	ft_putstr(dest);
	ft_putchar('\n');
	ft_putchar('\n');

	free(dest2);
	free(src2);

	return (0);
}
