#include <string.h>
#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb *= -1;
	}
	if (nb > 10)
		putnbr(nb / 10);
	ft_putchar(nb % 10 + '0');
}

char	*ft_strncat(char *dest, char *src, int nb);

int	main(int argc, char **argv)
{
	(void)argc;

	char *dest = (char*)malloc(sizeof (char) * (strlen(argv[1]) + strlen(argv[2])) + 1);
	strcpy(dest, argv[1]);
	char *src = (char*)malloc(sizeof (char) * strlen(argv[2]) + 1);
	strcpy(src, argv[2]);
	int nb = atoi(argv[3]);

	char *dest2 = (char*)malloc(sizeof (char) * (strlen(argv[1]) + strlen(argv[2])) + 1);
	strcpy(dest2, argv[1]);
	char *src2 = (char*)malloc(sizeof (char) * strlen(argv[2]) + 1);
	strcpy(src2, argv[2]);
	int nb2 = atoi(argv[3]);

	ft_putstr("DEST = ");
	ft_putstr(argv[1]);
	ft_putstr(", ");
	ft_putstr("SRC = ");
	ft_putstr(argv[2]);
	ft_putstr("\n");

	ft_putstr("STRNCAT = ");
	ft_putstr(strncat(dest, src, nb));
	ft_putstr("\n");

	ft_putstr("FT_STRNCAT = ");
	ft_putstr(ft_strncat(dest2, src2, nb2));
	ft_putstr("\n");

	free(dest2);
	free(src2);
	return (0);
}
