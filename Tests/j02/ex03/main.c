#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

void	ft_is_negative(int n);

int		main(void)
{
	ft_putstr("TEST '-1' (N) = ");
	ft_is_negative(-1);
	ft_putchar('\n');

	ft_putstr("TEST '0' (P) = ");
	ft_is_negative(0);
	ft_putchar('\n');

	ft_putstr("TEST '1' (P) = ");
	ft_is_negative(1);
	ft_putchar('\n');
}
