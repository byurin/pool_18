/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 18:30:10 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/08 09:56:04 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr(int nb);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int		main(void)
{
	ft_putstr("TEST '-2147483648' = ");
	ft_putnbr(-2147483648);
	ft_putchar('\n');

	ft_putstr("TEST '-1' = ");
	ft_putnbr(-1);
	ft_putchar('\n');

	ft_putstr("TEST '-10' = ");
	ft_putnbr(-10);
	ft_putchar('\n');

	ft_putstr("TEST '-100' = ");
	ft_putnbr(-100);
	ft_putchar('\n');

	ft_putstr("TEST '-123' = ");
	ft_putnbr(-123);
	ft_putchar('\n');

	ft_putstr("TEST '-1' = ");
	ft_putnbr(-1);
	ft_putchar('\n');

	ft_putstr("TEST '0' = ");
	ft_putnbr(0);
	ft_putchar('\n');

	ft_putstr("TEST '1' = ");
	ft_putnbr(1);
	ft_putchar('\n');

	ft_putstr("TEST '10' = ");
	ft_putnbr(10);
	ft_putchar('\n');

	ft_putstr("TEST 'a' = ");
	ft_putnbr('a');
	ft_putchar('\n');

	ft_putstr("TEST '+2147483647' = ");
	ft_putnbr(+2147483647);
	ft_putchar('\n');

	return (0);
}
