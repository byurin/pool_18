#include <stdio.h>

int ft_iterative_power(int nb, int power);

int	main(void)
{
	printf("-4 ^ -4 = %d\n", ft_iterative_power(-4, -4));
	printf("-4 ^ 4 = %d\n", ft_iterative_power(-4, 4));
	printf("-4 ^ 5 = %d\n", ft_iterative_power(-4, 5));
	printf("-5 ^ 4 = %d\n", ft_iterative_power(-5, 4));
	printf("-1 ^ 3 = %d\n", ft_iterative_power(-1, 3));
	printf("0 ^ 3 = %d\n", ft_iterative_power(0, 3));
	printf("1 ^ 3 = %d\n", ft_iterative_power(1, 3));
	printf("5 ^ 3 = %d\n", ft_iterative_power(5, 3));
	printf("7 ^ 4 = %d\n", ft_iterative_power(7, 4));
	printf("4 ^ -1 = %d\n", ft_iterative_power(4, -1));
	printf("4 ^ 0 = %d\n", ft_iterative_power(4, 0));
	printf("4 ^ 1 = %d\n", ft_iterative_power(4, 1));
	printf("4 ^ 2 = %d\n", ft_iterative_power(4, 2));
	return (0);
}
