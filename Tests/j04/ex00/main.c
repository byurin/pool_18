#include <stdio.h>

int ft_iterative_factorial(int nb);

int	main(void)
{
	printf("0 = %d\n", ft_iterative_factorial(0));
	printf("1 = %d\n", ft_iterative_factorial(1));
	printf("5 = %d\n", ft_iterative_factorial(5));
	printf("10 = %d\n", ft_iterative_factorial(10));
	printf("12 = %d\n", ft_iterative_factorial(12));
	printf("-12 = %d\n", ft_iterative_factorial(-12));
	printf("13 = %d\n", ft_iterative_factorial(13));
	return (0);
}
