#include <stdio.h>
#include <unistd.h>

int		ft_eight_queens_puzzle(void);

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	putnbr(int nb)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb *= -1;
	}
	if (nb > 10)
		putnbr(nb / 10);
	ft_putchar(nb % 10 + '0');
}

int		main(void)
{
	putnbr(ft_eight_queens_puzzle());
	return (0);
}
