#include <stdio.h>

int	ft_sqrt(int nb);

int	main(void)
{
	printf("1 = %d\n", ft_sqrt(1));
	printf("2 = %d\n", ft_sqrt(2));
	printf("3 = %d\n", ft_sqrt(3));
	printf("4 = %d\n", ft_sqrt(4));
	printf("5 = %d\n", ft_sqrt(5));
	printf("6 = %d\n", ft_sqrt(6));
	printf("7 = %d\n", ft_sqrt(7));
	printf("8 = %d\n", ft_sqrt(8));
	printf("9 = %d\n", ft_sqrt(9));
	printf("11 = %d\n", ft_sqrt(11));
	printf("23 = %d\n", ft_sqrt(23));
	printf("34 = %d\n", ft_sqrt(34));
	printf("46 = %d\n", ft_sqrt(46));
	printf("59 = %d\n", ft_sqrt(59));
	printf("67 = %d\n", ft_sqrt(67));
	printf("73 = %d\n", ft_sqrt(73));
	printf("84 = %d\n", ft_sqrt(84));
	printf("90 = %d\n", ft_sqrt(90));
	printf("101 = %d\n", ft_sqrt(101));
	printf("2077627561 = %d\n", ft_sqrt(2077627561));
	printf("2147483647 = %d\n", ft_sqrt(2147483646));
}
