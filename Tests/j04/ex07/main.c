#include <stdio.h>

int	ft_find_next_prime(int nb);

int	main(void)
{
	printf("-1 (0) = %d\n", ft_find_next_prime(-1));
	printf("0 (0) = %d\n", ft_find_next_prime(0));
	printf("1 (0) = %d\n", ft_find_next_prime(1));
	printf("2 (1) = %d\n", ft_find_next_prime(2));
	printf("3 (1) = %d\n", ft_find_next_prime(3));
	printf("4 (0) = %d\n", ft_find_next_prime(4));
	printf("5 (1) = %d\n", ft_find_next_prime(5));
	printf("6 (0) = %d\n", ft_find_next_prime(6));
	printf("7 (1) = %d\n", ft_find_next_prime(7));
	printf("8 (0) = %d\n", ft_find_next_prime(8));
	printf("9 (0) = %d\n", ft_find_next_prime(9));
	printf("10 (0) = %d\n", ft_find_next_prime(10));
	printf("11 (1) = %d\n", ft_find_next_prime(11));
	printf("13 (1) = %d\n", ft_find_next_prime(13));
	printf("17 (1) = %d\n", ft_find_next_prime(17));
	printf("19 (1) = %d\n", ft_find_next_prime(19));
	printf("23 (1) = %d\n", ft_find_next_prime(23));
	printf("22921 (1) = %d\n", ft_find_next_prime(22921));
	printf("50621 (0) = %d\n", ft_find_next_prime(50621));
	printf("122501 (1) = %d\n", ft_find_next_prime(122501));
	printf("241596 (0) = %d\n", ft_find_next_prime(241596));
	printf("241597 (1) = %d\n", ft_find_next_prime(241597));
	printf("241598 (0) = %d\n", ft_find_next_prime(241598));
	printf("250517 (0) = %d\n", ft_find_next_prime(250517));
	printf("355843 (0) = %d\n", ft_find_next_prime(355843));
	printf("2147483647 (1) = %d\n", ft_find_next_prime(2147483647));
}
