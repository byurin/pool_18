#include <unistd.h>

int		ft_eight_queens_puzzle_2(void);

void	ft_putchar(char c)
{
	write (1, &c, 1);
}

void	ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int		main(void)
{
	ft_eight_queens_puzzle_2();
	return (0);
}
