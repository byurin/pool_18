#include <stdio.h>
#include <string.h>

char	*ft_strrev(char *str);

int	main(int argc, char **argv)
{
	if (argc > 1)
	{
		argc--;
		while (argc > 0)
			printf("str = %s \n", ft_strrev(argv[argc--]));
	}
	else
		return (1);
	return (0);
}
