#include <stdio.h>

void	ft_ultimate_div_mod(int *a, int *b);

int	main(void)
{
	int value_a = 9;
	int value_b = 2;
	int *a = &value_a;
	int	*b = &value_b;
	ft_ultimate_div_mod(a, b);
	printf("TEST a '4' = %d\n", value_a);
	printf("TEST b '1' = %d\n", value_b);
	return(0);
}
