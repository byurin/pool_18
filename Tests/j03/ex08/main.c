#include <stdio.h>
#include <stdlib.h>

int	ft_atoi(char *str);

int	main(void)
{
	printf("-2147483649\n");
	printf("FT =%d\n", ft_atoi("-2147483649"));
	printf("ATOI =%d\n\n", atoi("-2147483649"));

	printf("-2147483648\n");
	printf("FT =%d\n", ft_atoi("-2147483648"));
	printf("ATOI =%d\n\n", atoi("-2147483648"));

	printf("2147483649\n");
	printf("FT =%d\n", ft_atoi("2147483649"));
	printf("ATOI =%d\n\n", atoi("2147483649"));

	printf("2147483648\n");
	printf("FT =%d\n", ft_atoi("2147483648"));
	printf("ATOI =%d\n\n", atoi("2147483648"));

	printf("2147483647\n");
	printf("FT =%d\n", ft_atoi("2147483647"));
	printf("ATOI =%d\n\n", atoi("2147483647"));

	printf("-0123456789\n");
	printf("FT =%d\n", ft_atoi("-0123456789"));
	printf("ATOI =%d\n\n", atoi("-0123456789"));

	printf("-10\n");
	printf("FT =%d\n", ft_atoi("-10"));
	printf("ATOI =%d\n\n", atoi("-10"));

	printf("+12\n");
	printf("FT =%d\n", ft_atoi("+12"));
	printf("ATOI =%d\n\n", atoi("+12"));

	printf("-1\n");
	printf("FT =%d\n", ft_atoi("-1"));
	printf("ATOI =%d\n\n", atoi("-1"));

	printf("000\n");
	printf("FT =%d\n", ft_atoi("000"));
	printf("ATOI =%d\n\n", atoi("000"));

	printf("a12\n");
	printf("FT =%d\n", ft_atoi("a12"));
	printf("ATOI =%d\n\n", atoi("a12"));

	printf("       21u\n");
	printf("FT =%d\n", ft_atoi("       21u"));
	printf("ATOI =%d\n\n", atoi("       21u"));

	printf("             -12\n");
	printf("FT =%d\n", ft_atoi("             -12"));
	printf("ATOI =%d\n\n", atoi("             -12"));

	printf("-1-12\n");
	printf("FT =%d\n", ft_atoi("-1-12"));
	printf("ATOI =%d\n\n", atoi("-1-12"));

	printf("--12\n");
	printf("FT =%d\n", ft_atoi("--12"));
	printf("ATOI =%d\n\n", atoi("--12"));

	printf("-+12\n");
	printf("FT =%d\n", ft_atoi("-+12"));
	printf("ATOI =%d\n\n", atoi("-+12"));

	printf("''\n");
	printf("FT =%d\n", ft_atoi(""));
	printf("ATOI =%d\n\n", atoi(""));

	printf("\\0\n");
	printf("FT =%d\n", ft_atoi("\0"));
	printf("ATOI =%d\n\n", atoi("\0"));

	printf("%c\n", 177);
	printf("FT =%d\n", ft_atoi("\\177"));
	printf("ATOI =%d\n\n", atoi("\\177"));

	printf("-01234567890123456789\n");
	printf("FT =%d\n", ft_atoi("-01234567890123456789"));
	printf("ATOI =%d\n\n", atoi("-01234567890123456789"));

	printf("   \\0    1\n");
	printf("FT =%d\n", ft_atoi("   \0    1"));
	printf("ATOI =%d\n\n", atoi("   \0    1"));

	printf("\\f\\n\\r\\t\\v 1\n");
	printf("FT =%d\n", ft_atoi("\f\n\r\t\v 1"));
	printf("ATOI =%d\n\n", atoi("\f\n\r\t\v 1"));

	return (0);
}
