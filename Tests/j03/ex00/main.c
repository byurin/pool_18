#include <stdio.h>

void	ft_ft(int *nbr);

int	main(void)
{
	int value = 1;
	int *ptr = &value;
	ft_ft(ptr);
	printf("%d\n", *ptr);
	printf("%d\n", value);
	return (0);
}
