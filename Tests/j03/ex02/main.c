#include <stdio.h>

int		ft_swap(int *a, int *b);

int		main(void)
{
	int nb1 = 123;
	int nb2 = -235;
	int *a = &nb1;
	int *b = &nb2;
	ft_swap(a, b);
	return (0);
}
