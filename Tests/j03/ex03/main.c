#include <stdio.h>

void	ft_div_mod(int a, int b, int *div, int *mod);

int	main(void)
{
	int a = 9;
	int b = 2;
	int div_value = 0;
	int mod_value = 0;
	int *div = &div_value;
	int	*mod = &mod_value;
	ft_div_mod(a, b, div, mod);
	printf("TEST div '4' = %d\n", div_value);
	printf("TEST mod '1' = %d\n", mod_value);
	return(0);
}
