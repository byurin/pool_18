#include <stdio.h>

int		ft_strlen(char *str);

int	main(void)
{

	printf("TEST '0' = %d\n", ft_strlen(""));
	printf("TEST '0' = %d\n", ft_strlen("\0Hello World !"));
	printf("TEST '5' = %d\n", ft_strlen("Hello\0 World !"));
	printf("TEST '13' = %d\n", ft_strlen("Hello World !"));
	printf("TEST '10' = %d\n", ft_strlen("0123456789"));
	return (0);
}
