#include <unistd.h>

void	ft_putstr(char *str);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int main(void)
{
	ft_putstr("");
	ft_putstr("\0");
	ft_putstr("!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}TESTSPACE ?\n\0SHOULD'VE STOPPED");
	ft_putstr("\n");
	return (0);
}
