#include <stdio.h>

int	*ft_sort_integer_table(int *tab, int size);

void	display(int *tab, int size)
{
	int i = 0;
	while (i < size)
		printf("%d ", tab[i++]);
	printf("\n");
}

int	main(void)
{
	int	tab[] = {3,2,7,3,1,4,7,2};
	int	tab2[] = {7,3,2,7,1,4,7,7,3};
	int	tab3[] = {4356, 4357, 0, 10, -10, 14, 13, 12, -100, -247};
	int	tab4[] = {-211, 4356, 4357, 0, 10, -10, 14, 13, 12, -100, -247};
	int tab5[] = {0};
	int tab6[] = {};
	int tab7[] = {-2147483648, 0, +2147483647};
	int tab8[] = {1, 1, 1, 1, 1};
	int *tabr;

	tabr = ft_sort_integer_table(tab, 8);
	display(tabr, 8);

	tabr = ft_sort_integer_table(tab2, 9);
	display(tabr, 9);

	tabr = ft_sort_integer_table(tab3, 10);
	display(tabr, 10);

	tabr = ft_sort_integer_table(tab4, 11);
	display(tabr, 11);

	tabr = ft_sort_integer_table(tab5, 1);
	display(tabr, 1);

	tabr = ft_sort_integer_table(tab6, 0);
	display(tabr, 0);

	tabr = ft_sort_integer_table(tab7, 3);
	display(tabr, 3);

	tabr = ft_sort_integer_table(tab8, 5);
	display(tabr, 5);
	return (0);
}
