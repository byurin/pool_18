/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_next_prime.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 17:56:18 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/19 16:58:16 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_prime(int nb, int prime)
{
	if (nb < 2)
		return (0);
	if (((nb % prime == 0 || nb % 2 == 0) && prime < nb) || (prime > 46341))
		return (0);
	if (prime * prime < nb && prime < 46341)
		nb = is_prime(nb, prime + 1);
	return (nb);
}

int		ft_find_next_prime(int nb)
{
	if (nb < 2)
		return (2);
	if (is_prime(nb, 2) <= 0)
	{
		while (is_prime(nb, 2) <= 0)
			nb++;
	}
	return (nb);
}
