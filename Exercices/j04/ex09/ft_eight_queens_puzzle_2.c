/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle_2.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 10:17:39 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/13 15:46:16 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	print_queens(int *board)
{
	int i;

	i = 0;
	while (i++ < 8)
		ft_putchar(board[i] + '0');
	ft_putchar('\n');
}

int		check_queen(int *board, int queen)
{
	int i;
	int offset;

	i = queen - 1;
	offset = queen - i;
	if (queen == 1)
		return (0);
	while (i > 0)
	{
		if (board[i] == board[queen])
			return (1);
		else if (board[i] == (board[queen] - offset))
			return (1);
		else if (board[i] == (board[queen] + offset))
			return (1);
		else if (board[queen] > 8)
			return (1);
		i--;
		offset++;
	}
	return (0);
}

int		check_board(int *board)
{
	int i;

	i = 8;
	while (i > 0)
	{
		if (check_queen(board, i) == 1)
			return (1);
		i--;
	}
	return (0);
}

int		*place_queens(int *board, int queen)
{
	if (board[0] <= 8)
	{
		if (check_board(board) == 0)
		{
			print_queens(board);
			board[8]++;
		}
		else if (board[queen] >= 8 && check_queen(board, queen) == 1)
		{
			board[queen] = 1;
			queen--;
			if (queen == 1 && board[queen] >= 8)
				return (board);
			else
				board[queen]++;
		}
		else if (check_queen(board, queen) == 0 && queen != 8)
			queen++;
		else if (check_queen(board, queen) == 1 && board[queen] <= 8)
			board[queen]++;
		return (place_queens(board, queen));
	}
	return (board);
}

void	ft_eight_queens_puzzle_2(void)
{
	int		i;
	int		board[8];

	i = 1;
	while (i++ <= 8)
		board[i] = 1;
	place_queens(board, 1);
}
