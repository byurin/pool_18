/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 15:10:31 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/19 16:50:38 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_prime(int nb, int prime)
{
	if (nb < 2)
		return (0);
	if (((nb % prime == 0 || nb % 2 == 0) && prime < nb) || (prime > 46341))
		return (0);
	if (prime * prime < nb && prime < 46341)
		nb = is_prime(nb, prime + 1);
	return (nb);
}

int		ft_is_prime(int nb)
{
	return (is_prime(nb, 2) == 0 ? 0 : 1);
}
