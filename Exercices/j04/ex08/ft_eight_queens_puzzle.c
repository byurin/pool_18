/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/13 10:17:39 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/14 20:38:24 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		check_queen(int *board, int queen)
{
	int i;
	int offset;

	i = queen - 1;
	offset = queen - i;
	if (queen == 1)
		return (0);
	while (i > 0)
	{
		if (board[i] == board[queen])
			return (1);
		else if (board[i] == (board[queen] - offset))
			return (1);
		else if (board[i] == (board[queen] + offset))
			return (1);
		else if (board[queen] > 8)
			return (1);
		i--;
		offset++;
	}
	return (0);
}

int		check_board(int *board)
{
	int i;

	i = 8;
	while (i > 0)
	{
		if (check_queen(board, i) == 1)
			return (1);
		i--;
	}
	return (0);
}

int		place_queens(int *board, int queen)
{
	int res;

	res = 0;
	if (board[0] <= 8)
	{
		if (check_board(board) == 0)
		{
			res++;
			board[8]++;
		}
		else if (board[queen] >= 8 && check_queen(board, queen) == 1)
		{
			board[queen] = 1;
			queen--;
			if (queen == 1 && board[queen] >= 8)
				return (res);
			board[queen]++;
		}
		else if (check_queen(board, queen) == 0 && queen != 8)
			queen++;
		else if (check_queen(board, queen) == 1 && board[queen] <= 8)
			board[queen]++;
		return (res + place_queens(board, queen));
	}
	return (res);
}

int		ft_eight_queens_puzzle(void)
{
	int		i;
	int		board[8];

	i = 1;
	while (i++ <= 8)
		board[i] = 1;
	return (place_queens(board, 1));
}
