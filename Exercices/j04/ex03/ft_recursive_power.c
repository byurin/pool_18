/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 16:13:59 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/15 22:26:30 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		recursion(int nb, int power)
{
	if (power == 0)
		return (1);
	if (power < 0 || nb == 0)
		return (0);
	else if (nb == 1)
		return (1);
	else
		nb = (nb * recursion(nb, power - 1));
	return (nb);
}

int		ft_recursive_power(int nb, int power)
{
	int mixin;

	mixin = nb;
	nb = recursion(nb, power);
	if (mixin < 0 && nb > 0)
		return (nb * -1);
	return (nb);
}
