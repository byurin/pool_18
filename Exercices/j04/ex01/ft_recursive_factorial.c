/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 15:26:10 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/09 16:03:11 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_factorial(int nb)
{
	if (nb < 2 || nb > 12)
		return ((nb < 0 || nb > 12) ? 0 : 1);
	else if (nb > 0)
		nb = (nb * ft_recursive_factorial(nb - 1));
	return (nb);
}
