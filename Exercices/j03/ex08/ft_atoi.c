/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 15:29:32 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/09 13:51:16 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_atoi(char *str)
{
	int result;
	int flag;

	result = 0;
	flag = 1;
	while ((*str >= 9 && *str <= 13) || *str == 32)
		str++;
	flag = *str == '-' ? -1 : 1;
	if (*str == '-' || *str == '+')
		str++;
	while (*str && (*str >= '0' && *str <= '9'))
		result = (result * 10) + (*str++ - '0');
	return (result * flag);
}
