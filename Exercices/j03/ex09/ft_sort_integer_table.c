/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 15:56:48 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/09 11:56:37 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	swap(int *tab, int pos)
{
	tab[pos] ^= tab[pos + 1];
	tab[pos + 1] ^= tab[pos];
	tab[pos] ^= tab[pos + 1];
}

int		*ft_sort_integer_table(int *tab, int size)
{
	int i;

	i = -1;
	size--;
	while (size > 0)
	{
		while (i++ < size - 1)
			if (tab[i] > tab[i + 1])
				swap(tab, i);
		i = -1;
		size--;
	}
	return (tab);
}
