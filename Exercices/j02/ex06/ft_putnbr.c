/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bopopovi <bopopovi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 17:36:43 by bopopovi          #+#    #+#             */
/*   Updated: 2018/02/08 10:10:38 by bopopovi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_printer(int nb)
{
	if (nb > 9)
		ft_printer(nb / 10);
	ft_putchar((nb % 10) + '0');
}

void	ft_putnbr(int nb)
{
	int is_min;

	is_min = 0;
	if (nb == -2147483648)
		is_min = 1;
	if (nb < 0)
	{
		ft_putchar('-');
		is_min == 0 ? (nb *= -1) : (nb = 214748364);
	}
	ft_printer(nb);
	if (is_min == 1)
		ft_putchar('8');
}
